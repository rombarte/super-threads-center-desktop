﻿using System.ComponentModel.DataAnnotations;

namespace Projekt_DotNet
{
    public class User
    {
        [Key]
        public int id { get; set; }
        //[Index(IsUnique = true)]
        public string username { get; set; }
        public string password { get; set; }
        public int accountType { get; set; }

        public override string ToString()
        {
            return username;
        }
    }
}