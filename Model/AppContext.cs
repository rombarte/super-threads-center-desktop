﻿using Projekt_DotNet.Model;
using System.Data.Entity;

namespace Projekt_DotNet
{
    public class AppContext : DbContext
    {
        public DbSet<User> UserSet { get; set; }
        public DbSet<Post> PostSet { get; set; }
        public DbSet<Thread> ThreadSet { get; set; }

        public AppContext() : base()
        {
            this.Configuration.LazyLoadingEnabled = true;
        }

        public AppContext(string connectionString) : base(connectionString)
        {
            this.Configuration.LazyLoadingEnabled = true;
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            /*
            modelBuilder.Entity<Post>()
            .HasMany<User>(c => c.user)
            .WithOptional(x => x.id)
            .WillCascadeOnDelete(true);
            */
        }
    }
}