﻿using Projekt_DotNet.Model;
using System;
using System.Windows;

namespace Projekt_DotNet
{
    /// <summary>
    /// Interaction logic for PostAdd.xaml
    /// </summary>
    public partial class PostAdd : Window
    {
        AppContext context = null;
        Thread thisThread = null;
        User thisUser = null;

        public PostAdd(AppContext context, Thread thisThread, User thisUser)
        {
            this.context = context;
            this.thisThread = thisThread;
            this.thisUser = thisUser;

            InitializeComponent();

            date.Text = DateTime.UtcNow.ToString();
        }

        private void button_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Post newPost = new Post { id = 1, content = textBox.Text, date = date.Text, user = thisUser };
                thisThread.posts.Add(newPost);
                context.SaveChanges();
            }

            catch(Exception exc)
            {
                MessageBox.Show(exc.Message);
            }

            this.Close();
        }
    }
}