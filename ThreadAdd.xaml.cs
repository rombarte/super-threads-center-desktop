﻿using Projekt_DotNet.Model;
using System;
using System.Windows;
using System.Windows.Documents;

namespace Projekt_DotNet
{
    /// <summary>
    /// Interaction logic for ThreadAdd.xaml
    /// </summary>
    public partial class ThreadAdd : Window
    {
        AppContext db = null;
        User thisUser = null;

        public ThreadAdd(AppContext db, User thisUser)
        {
            this.db = db;
            this.thisUser = thisUser;

            InitializeComponent();

            date.Text = DateTime.UtcNow.ToString();
        }

        private void button_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                db.ThreadSet.Add(new Thread { id = 1, title = title.Text, content = new TextRange(content.Document.ContentStart, content.Document.ContentEnd).Text, date = date.Text, user = thisUser });
                db.SaveChanges();
            }

            catch(Exception exc)
            {
                MessageBox.Show(exc.Message);
            }

            this.Close();
        }
    }
}
