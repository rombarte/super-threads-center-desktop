﻿using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Windows;

namespace Projekt_DotNet
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        AppContext db = null;

        public string passwordEncode(string password)
        {
            return new string(UTF8Encoding.ASCII.GetChars(new SHA1CryptoServiceProvider().ComputeHash(UTF8Encoding.ASCII.GetBytes(password))));
        }

        public string passwordDecode(string hashcode)
        {
            return new string(UTF8Encoding.ASCII.GetChars(new SHA1CryptoServiceProvider().ComputeHash(UTF8Encoding.ASCII.GetBytes(hashcode))));
        }

        public MainWindow()
        {
            InitializeComponent();

            db = new AppContext("Data Source=(LocalDB)\\MSSQLLocalDB;AttachDbFilename=C:\\Users\\Public\\ForumDB.mdf;Integrated Security=True;Connect Timeout=30");

            // Create example users if not exists
            if (!db.UserSet.Any(item => item.id == 1))
            {
                db.UserSet.Add(new User { id = 1, username = "admin", password = passwordEncode("pass1"), accountType = 1 });
                db.UserSet.Add(new User { id = 2, username = "user", password = passwordEncode("pass2"), accountType = 2 });

                db.SaveChanges();

                MessageBox.Show("New database created!");
            }
        }

        private void button_Click(object sender, RoutedEventArgs e)
        {
            // Login to system
            if(username.Text.Length != 0 && password.Password.Length != 0)
            {
                string passHash = passwordEncode(password.Password);
                User thisUser = db.UserSet.SingleOrDefault(user => user.username == username.Text && user.password == passHash);

                if (thisUser != null)
                {
                    // Log OK
                    ThreadList threadList = new ThreadList(this, db, thisUser);
                    this.Hide();
                    threadList.Show();
                    //this.Show();
                }

                else
                {
                    // Log WRONG
                    MessageBox.Show("Password is wrong.");
                }
            }

            else
            {
                MessageBox.Show("Username or password is empty!");
            }
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            // Close window
            db.Dispose();
        }

        private void MenuItem_Click(object sender, RoutedEventArgs e)
        {
            // Close
            this.Close();
        }

        private void MenuItem_Click_1(object sender, RoutedEventArgs e)
        {
            // Register
            Register register = new Register(db);
            register.ShowDialog();
        }

        private void MenuItem_Click_2(object sender, RoutedEventArgs e)
        {
            // About
            MessageBox.Show("App created by Bartłomiej Romanek.");
        }
    }
}