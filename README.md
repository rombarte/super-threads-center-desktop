# Super Threads Center Desktop
Simple discussion board written in C# for Windows Desktop - version for Windows Desktop

## License
All files in this repository are available under the MIT license. You can find more information and a link to the license content on the [Wikipedia](https://en.wikipedia.org/wiki/MIT_License) page.