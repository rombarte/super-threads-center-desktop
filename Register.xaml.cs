﻿using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Windows;

namespace Projekt_DotNet
{
    /// <summary>
    /// Interaction logic for Register.xaml
    /// </summary>
    public partial class Register : Window
    {
        AppContext context = null;

        public Register(AppContext context)
        {
            this.context = context;

            InitializeComponent();
        }

        public string passwordEncode(string password)
        {
            return new string(UTF8Encoding.ASCII.GetChars(new SHA1CryptoServiceProvider().ComputeHash(UTF8Encoding.ASCII.GetBytes(password))));
        }

        private void button_Click(object sender, RoutedEventArgs e)
        {
            if (textBox.Text.Length != 0 && textBox1.Password.Length != 0)
            {
                string password = passwordEncode(textBox1.Password);

                // User not exists
                if (context.UserSet.Any(user => user.username == textBox.Text) == false)
                {
                    try
                    {
                        context.UserSet.Add(new User { id = 1, username = textBox.Text, password = password, accountType = 2 });
                        context.SaveChanges();

                        this.Close();
                    }
                    catch
                    {
                        MessageBox.Show("User exists! Create new once.");
                    }
                }

                else
                {
                    MessageBox.Show("User exists! Create new once.");
                }
            }

            else
            {
                MessageBox.Show("Username or password is empty!");
            }
            
        }
    }
}