﻿using System;
using System.Linq;
using System.Windows;

namespace Projekt_DotNet
{
    /// <summary>
    /// Interaction logic for UserList.xaml
    /// </summary>
    public partial class UserList : Window
    {
        AppContext db = null;

        public UserList(AppContext context)
        {
            db = context;
            InitializeComponent();
            dataGrid.ItemsSource = db.UserSet.ToList();
        }

        private void delete_Click(object sender, RoutedEventArgs e)
        {
            // Delete User
            try
            {
                User selUser = (User)dataGrid.SelectedItem;
                var record = db.UserSet.SingleOrDefault(item => item.id == selUser.id);
                db.UserSet.Remove(record);
                db.SaveChanges();

                dataGrid.ItemsSource = null;
                dataGrid.ItemsSource = db.UserSet.ToList();
            }

            catch (Exception exc)
            {
                MessageBox.Show(exc.Message + exc.InnerException.InnerException.Message);
            }
        }
    }
}