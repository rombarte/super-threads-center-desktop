﻿using Projekt_DotNet.Model;
using System;
using System.Data.Entity;
using System.Linq;
using System.Windows;
using System.Windows.Input;

namespace Projekt_DotNet
{
    /// <summary>
    /// Interaction logic for ThreadList.xaml
    /// </summary>
    public partial class ThreadList : Window
    {
        MainWindow parentWindow;
        AppContext db = null;
        User thisUser = null;

        public ThreadList(MainWindow window, AppContext db, User thisUser)
        {
            parentWindow = window;
            this.db = db;
            this.thisUser = thisUser;

            InitializeComponent();

            try
            {
                dataGrid.ItemsSource = db.ThreadSet.Include(item => item.posts).Include(item => item.user).ToList();

                if (thisUser.accountType != 1)
                {
                    button2.IsEnabled = false;
                    users.IsEnabled = false;
                }
            }

            catch (Exception exc)
            {
                MessageBox.Show(exc.Message);
            }
            
        }

        private void button_Click(object sender, RoutedEventArgs e)
        {
            // Add Thread
            ThreadAdd threadAdd = new ThreadAdd(db, thisUser);
            threadAdd.ShowDialog();
            dataGrid.ItemsSource = db.ThreadSet.Include(item => item.posts).Include(item => item.user).ToList();
        }

        private void button1_Click(object sender, RoutedEventArgs e)
        {
            // Refresh
            dataGrid.ItemsSource = db.ThreadSet.Include(item => item.posts).Include(item => item.user).ToList();
        }

        private void dataGrid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            // Open Thread
            try
            {
                Thread thisThread = (Thread)dataGrid.SelectedItem;
                ThreadWindow thread = new ThreadWindow(this, db, thisThread, thisUser);
                this.Hide();
                thread.ShowDialog();
                //this.ShowDialog();
                dataGrid.ItemsSource = db.ThreadSet.Include(item => item.posts).Include(item => item.user).ToList();
            }

            catch(Exception exc)
            {
                MessageBox.Show(exc.Message);
            }
            
        }

        private void button2_Click(object sender, RoutedEventArgs e)
        {
            // Delete
            Thread thisThread = (Thread)dataGrid.SelectedItem;

            try
            {
                var record = db.ThreadSet.Include(item => item.posts).Include(item => item.user).SingleOrDefault(item => item.id == thisThread.id);
                db.ThreadSet.Remove(record);
                db.SaveChanges();

                dataGrid.ItemsSource = db.ThreadSet.Include(item => item.posts).Include(item => item.user).ToList();
            }

            catch (Exception exc)
            {
                MessageBox.Show(exc.Message);
            }
        }

        private void filter_TextChanged(object sender, System.Windows.Controls.TextChangedEventArgs e)
        {
            dataGrid.ItemsSource = db.ThreadSet.Include(item => item.posts).Include(item => item.user).Where(item => item.title.Contains(filter.Text) == true).ToList();
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            parentWindow.Show();
        }

        private void users_Click(object sender, RoutedEventArgs e)
        {
            UserList userList = new UserList(db);
            userList.ShowDialog();
        }
    }
}
