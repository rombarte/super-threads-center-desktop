﻿using Hardcodet.Wpf.TaskbarNotification;
using Projekt_DotNet.Model;
using System;
using System.Data.Entity;
using System.Linq;
using System.Speech.Synthesis;
using System.Timers;
using System.Windows;
using System.Windows.Documents;
using System.Windows.Input;

namespace Projekt_DotNet
{
    /// <summary>
    /// Interaction logic for Thread.xaml
    /// </summary>
    public partial class ThreadWindow : Window
    {
        ThreadList parentWindow;
        AppContext context = null;
        Thread thisThread = null;
        User thisUser = null;
        Timer refreshTimer = null;
        int oldPostCount;

        public ThreadWindow(ThreadList window, AppContext context, Thread thread, User thisUser)
        {
            parentWindow = window;
            this.context = context;
            thisThread = thread;
            this.thisUser = thisUser;

            InitializeComponent();

            textBlock.Text = thisThread.title;
            textBlock1_Copy.Text = "Thread date: " + thisThread.date;

            content.Document.Blocks.Clear();
            content.Document.Blocks.Add(new Paragraph(new Run(thisThread.content)));

            dataGrid.ItemsSource = thisThread.posts;

            if(thisUser.accountType != 1)
            {
                button2.IsEnabled = false;
                button3.IsEnabled = false;
            }
        }

        private void button_Click(object sender, RoutedEventArgs e)
        {
            // Add post
            PostAdd postAdd = new PostAdd(context, thisThread, thisUser);
            postAdd.ShowDialog();
            dataGrid.ItemsSource = null;
            dataGrid.ItemsSource = thisThread.posts;
        }

        private void dataGrid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            // Open post
            Post thisPost = (Post)dataGrid.SelectedItem;

            try
            {
                SpeechSynthesizer speechSynthesizer = new SpeechSynthesizer();
                speechSynthesizer.SpeakAsync(thisPost.content);

                if (MessageBox.Show(thisPost.content) == MessageBoxResult.OK)
                {
                    speechSynthesizer.Pause();
                }
            }

            catch (Exception exc)
            {
                MessageBox.Show(exc.Message);
            }
        }

        private void button1_Click(object sender, RoutedEventArgs e)
        {
            // Refresh
            dataGrid.ItemsSource = null;
            dataGrid.ItemsSource = thisThread.posts;
        }

        private void button2_Click(object sender, RoutedEventArgs e)
        {
            // Delete
            try
            {
                var record = context.ThreadSet.Include(item => item.posts).SingleOrDefault(item => item.id == thisThread.id);
                context.ThreadSet.Remove(record);
                context.SaveChanges();

                dataGrid.ItemsSource = context.ThreadSet.Include(item => item.posts).ToList();
                this.Close();
            }

            catch (Exception exc)
            {
                MessageBox.Show(exc.Message);
            }
        }

        private void TaskbarIcon_TrayMouseDoubleClick(object sender, RoutedEventArgs e)
        {
            if (this.IsVisible)
            {
                oldPostCount = context.ThreadSet.Include(item => item.posts).SingleOrDefault(item => item.id == thisThread.id).posts.Count;
                
                refreshTimer = new Timer();
                refreshTimer.Interval = 1000;
                refreshTimer.Elapsed += OnTimedEvent;
                refreshTimer.AutoReset = true;
                refreshTimer.Enabled = true;

                dataGrid.ItemsSource = null;
                this.Hide();
            }

            else
            {
                try
                {
                    refreshTimer.Stop();
                    dataGrid.ItemsSource = thisThread.posts;
                }
                
                catch(Exception exc)
                {
                    MessageBox.Show(exc.Message);
                }

                this.Show();
            }
        }

        private void OnTimedEvent(object sender, ElapsedEventArgs e)
        {
            try
            {
                var postsCount = context.ThreadSet.Include(item => item.posts).SingleOrDefault(item => item.id == thisThread.id);
                if (oldPostCount != postsCount.posts.Count)
                {
                    refreshTimer.Stop();
                    MessageBox.Show("Someone added new post!");
                }
            }

            catch(Exception exc)
            {
                refreshTimer.Stop();
                MessageBox.Show(exc.Message);
            }
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            TaskbarIcon tb = (TaskbarIcon)this.FindName("tb");
            tb.Dispose();
            parentWindow.Show();
        }

        private void button3_Click(object sender, RoutedEventArgs e)
        {
            // Delete post
            Post thisPost = (Post)dataGrid.SelectedItem;

            try
            {
                var record = context.PostSet.Include(item => item.user).SingleOrDefault(item => item.id == thisPost.id);
                context.PostSet.Remove(record);
                context.SaveChanges();

                dataGrid.ItemsSource = null;
                dataGrid.ItemsSource = thisThread.posts;
            }

            catch (Exception exc)
            {
                MessageBox.Show(exc.Message);
            }
        }

        private void button4_Click(object sender, RoutedEventArgs e)
        {
            oldPostCount = context.ThreadSet.Include(item => item.posts).SingleOrDefault(item => item.id == thisThread.id).posts.Count;

            refreshTimer = new Timer();
            refreshTimer.Interval = 1000;
            refreshTimer.Elapsed += OnTimedEvent;
            refreshTimer.AutoReset = true;
            refreshTimer.Enabled = true;

            dataGrid.ItemsSource = null;
            this.Hide();
        }
    }
}